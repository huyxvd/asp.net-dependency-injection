using Microsoft.AspNetCore.Mvc;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers();
builder.Services.AddSingleton<B>();

var app = builder.Build();
app.MapControllers();
app.Run();

[ApiController]
[Route("[controller]/[action]")]
public class BooksController : ControllerBase
{
    private readonly B _b;

    public BooksController(B b)
    {
        _b = b;
    }
    [HttpGet]
    public string Hi() => _b.GetData();
}

public class B
{
    public B()
    {
        Console.WriteLine("constructor");
    }
    public string GetData()
    {
        // calculated data
        return "hahahah";
    }
}
